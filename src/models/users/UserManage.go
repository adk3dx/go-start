package users

import (
	"../DB"
	"database/sql"
)

var db *sql.DB = DB.GetInstance()

func UserExists(login string) bool {
	stmt, err := db.Prepare(`SELECT count(*) AS quantity FROM users WHERE login = $1`)
	defer stmt.Close()
	if err != nil {
		panic(err)
	}
	var quantity int
	stmt.QueryRow(login).Scan(&quantity)

	return quantity > 0
}

func CreateUser(user *User) (id int) {
	var sql string = `INSERT INTO users (nickname, login, password)` +
		` VALUES ($1, $2, $3)` +
		` RETURNING id`
	stmt, err := db.Prepare(sql)
	defer stmt.Close()
	if err != nil {
		panic(err)
	}

	stmt.QueryRow(user.nickName, user.login, user.password).Scan(&id)

	return id
}

func SelectUserList(limit, offset int) map[int]User {
	rows, err := db.Query("SELECT * FROM users LIMIT $1 OFFSET $2", limit, offset)
	if err != nil {
		panic(err)
	}
	var userList = make(map[int]User)
	for rows.Next() {
		var user = User{}
		rows.Scan(&user.id, &user.nickName, &user.login, &user.password)
		userList[user.id] = user
	}
	return userList
}

func SelectUserByLogin(login string) (user User) {
	rows, err := db.Query("SELECT * FROM users WHERE login = $1", login)
	if err != nil {
		panic(err)
	}
	rows.Next()
	rows.Scan(&user.id, &user.nickName, &user.login, &user.password)
	return user
}
