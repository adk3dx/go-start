package users

import (
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	id       int
	nickName string
	login    string
	password string
}

func NewUser(nickName, login, password string) *User {
	return &User{
		nickName: nickName,
		login:    login,
		password: password,
	}
}

func (user *User) GetId() int {
	return user.id
}

func (user *User) GetNickName() string {
	return user.nickName
}

func (user *User) GetLogin() string {
	return user.login
}

func HashPassword(password string) (string, error) {
	pass, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return string(pass), err
}

func (user *User) VerifyPassword(password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(user.password), []byte(password))
	return err == nil
}
