package DB

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
)

func params() string {
	return fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		"127.0.0.1", 5433, "postgres", "123", "postgres") //"BestChatStore"
}

var err error
var db *sql.DB

func GetInstance() *sql.DB {
	if db == nil {
		db, err = sql.Open("postgres", params())
		if err != nil {
			panic(err)
		}
	}
	return db
}
