package main

import (
	"fmt"
	"html/template"
	"net/http"

	"../models/DB"
	"../models/users"
)

func loginHandler(writer http.ResponseWriter, request *http.Request) {
	tpl, err := template.ParseFiles("src/view/login.html", "src/view/header.html", "src/view/footer.html")
	if err != nil {
		fmt.Fprintf(writer, err.Error())
	} else {
		tpl.ExecuteTemplate(writer, "login", nil)
	}
}

func registrationHandler(writer http.ResponseWriter, request *http.Request) {
	tpl, err := template.ParseFiles("src/view/registration.html", "src/view/header.html", "src/view/footer.html")
	if err != nil {
		fmt.Fprintf(writer, err.Error())
	} else {
		tpl.ExecuteTemplate(writer, "registration", nil)
	}
}

func createUserHandler(writer http.ResponseWriter, request *http.Request) {
	if request.Method != http.MethodPost {
		return
	}
	nickName := request.FormValue("nickName")
	login := request.FormValue("login")

	if users.UserExists(login) {
		http.Redirect(writer, request, "/registration", http.StatusOK)
		return
	}

	password, err := users.HashPassword(request.FormValue("password"))
	if err != nil {
		fmt.Fprintf(writer, "wrong data received")
	}
	user := users.NewUser(nickName, login, password)
	users.CreateUser(user)

	http.Redirect(writer, request, "/main", http.StatusOK)
}

func authHandler(writer http.ResponseWriter, request *http.Request) {
	if request.Method != http.MethodPost {
		return
	}

	login := request.FormValue("login")
	user := users.SelectUserByLogin(login)
	if user.GetId() == 0 {
		fmt.Fprintf(writer, "wrong data received")
	}
	password := request.FormValue("password")
	if !user.VerifyPassword(password) {
		http.Redirect(writer, request, "/", http.StatusOK)
		return
	}
	http.Redirect(writer, request, "/main", http.StatusOK)
}

func main() {

	db := DB.GetInstance()
	err := db.Ping()
	if err != nil {
		panic(err)
	}
	defer db.Close()

	users.SelectUserList(10, 0)

	http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("src/assets/"))))

	http.HandleFunc("/", loginHandler)
	http.HandleFunc("/registration", registrationHandler)
	http.HandleFunc("/login", authHandler)

	http.HandleFunc("/createUser", createUserHandler)

	http.HandleFunc("/main", func(writer http.ResponseWriter, request *http.Request) {
		fmt.Fprintf(writer, "hello amigo")
	})

	http.ListenAndServe(":3000", nil)
}
